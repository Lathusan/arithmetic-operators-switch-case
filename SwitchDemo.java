package com.eight;

import java.util.Scanner;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0 Jan 27, 2021
 **/

public class SwitchDemo {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		SwitchMethods switchMethods = new SwitchMethods();

		System.out.print("Enter the first number  : ");
		double numOne = scan.nextDouble();

		System.out.print("Enter the second number : ");
		double numSec = scan.nextDouble();

		System.out.println(
				"\nWhich opperation you want to use?\n1. Addition (+) \n2. Subtraction (-) \n3. Multiplication (x) \n4. Division (÷)");
		System.out.print("\nPlease select an option : ");
		int option = scan.nextInt();

		switch (option) {
		case 1:
			switchMethods.add(numOne, numSec);
			break;

		case 2:
			switchMethods.sub(numOne, numSec);
			break;
			
		case 3:
			switchMethods.mul(numOne, numSec);
			break;

		case 4:
			switchMethods.dev(numOne, numSec);
			break;

		default:
			System.err.println("Please select an correct option !!!");
			
			break;
		}

	}

}
