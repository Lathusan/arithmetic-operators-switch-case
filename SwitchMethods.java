package com.eight;

import java.util.Scanner;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0 Jan 27, 2021
 **/

public class SwitchMethods {

	Scanner scan = new Scanner(System.in);
	SwitchDemo switchDemo = new SwitchDemo();

	protected void add(double a, double b) {
		double add = a + b;
		System.out.println("Answer is : " + add);
		conMeth();
	}

	protected void sub(double a, double b) {
		double sub = a - b;
		System.out.println("Answer is : " + sub);
		conMeth();
	}

	protected void mul(double a, double b) {
		double mul = a * b;
		System.out.println("Answer is : " + mul);
		conMeth();
	}

	protected void dev(double a, double b) {
		double dev = a / b;
		System.out.println("Answer is : " + dev);
		conMeth();
	}

	private void conMeth() {
		System.out.println("\nDo you want to continue ? \n1. Yes \n2. No \nSelect an option : ");
		int conInput = scan.nextInt();

		switch (conInput) {
		case 1:
			switchDemo.main(null);
			break;
		case 2:
			System.out.println("Thanks for using system !!!");
			System.out.println("      Good Luck & Bye");
			System.exit(0);
			break;
		default:
			System.out.println("Good Luck & Bye");
			System.exit(0);
			break;
		}

	}

}
